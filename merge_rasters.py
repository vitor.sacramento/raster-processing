from osgeo import gdal
from tkinter.filedialog import askopenfilenames
from tkinter.filedialog import asksaveasfilename
import time


input_rasters = list(askopenfilenames(title='Selecione os rasters de entrada'))

output_raster = asksaveasfilename(title='Selecione o raster de saída com extensão')

def merge_rasters(input_rasters, output_raster):
    gdal.UseExceptions()

    gdal.SetConfigOption('NUM_THREADS', 'ALL_CPUS')

    start_time = time.time()

    options = ['-co', 'COMPRESS=LZW',
            '-co', 'PREDICTOR=1',
            '-co', 'TILED=YES',
            '-co', 'BIGTIFF=YES']

    g = gdal.Warp(output_raster, input_rasters, format="GTiff",
                options=options,callback=gdal.TermProgress_nocb)
    g = None

    time_spent = time.time() - start_time
    time_spent_format = '{:.2f}'.format(time_spent)

    print('Finished in ' + str(time_spent_format) + ' seconds')

merge_rasters(input_rasters, output_raster)