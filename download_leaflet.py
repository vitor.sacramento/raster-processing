import os
from samgeo import tms_to_geotiff
from osgeo import ogr
from tkinter.filedialog import askopenfilenames

shapefile_path = askopenfilenames(
    title="Select your ROI",
    filetypes=[('Shapefile', '*.shp')]
)

index = 0

for item in shapefile_path:
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataset = driver.Open(item)
    layer = dataset.GetLayer()

    # acquire the epsg from the shapefile
    EPSG = int(layer.GetSpatialRef().GetAttrValue('AUTHORITY',1))

    # get bounding box coordinates
    if EPSG == 4326 or EPSG == 4674:
        extent = layer.GetExtent()
        minx, maxx, miny, maxy = extent
    else:
        print('Shapefile not in WGS 84 Geographic(EPSG:4326)')
        exit()

    # print bounding box coordinates
    print(f"Bounding box coordinates: ({minx}, {miny}, {maxx}, {maxy})")

    bbox=[minx,miny,maxx,maxy]

    image = os.path.join(os.path.dirname(item),(str(index)+'_leaflet.tiff'))
    print(image)

    if not os.path.exists(os.path.dirname(image)):
        os.makedirs(os.path.dirname(image))
    
    try:    
        tms_to_geotiff(output=image, bbox=bbox, zoom=17, source='Satellite')
    except AttributeError as e:
        print(e)
    pass

    index += 1