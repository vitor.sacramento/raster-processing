import os
from osgeo import gdal, osr

# Input raster file in Albers South America projection
input_raster = "/home/vitor/Documentos/carbono/pa_br_carbonMap_improved_ds_oskar_chalmers.tif"

# Output raster file in WGS84 projection
output_raster = os.path.join(os.path.dirname(input_raster),os.path.basename(input_raster))+"_reporject"

# Define Albers South America projection
albers_proj4 = "+proj=aea +lat_1=-5 +lat_2=-42 +lat_0=-32 +lon_0=-60 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs"

# Define WGS84 projection
wgs84_proj4 = "+proj=longlat +datum=WGS84 +no_defs"

# Open input raster
input_dataset = gdal.Open(input_raster)

# Get input raster projection
input_projection = osr.SpatialReference()
input_projection.ImportFromProj4(albers_proj4)

# Create WGS84 projection
output_projection = osr.SpatialReference()
output_projection.ImportFromProj4(wgs84_proj4)

# Create transformation from Albers South America to WGS84
transform = osr.CoordinateTransformation(input_projection, output_projection)

# Get input raster dimensions
width = input_dataset.RasterXSize
height = input_dataset.RasterYSize

# Create output raster
output_driver = gdal.GetDriverByName("GTiff")
output_dataset = output_driver.Create(output_raster, width, height, 1, gdal.GDT_Float32)

# Set output raster projection
output_dataset.SetProjection(output_projection.ExportToWkt())

# Set output raster geotransform
output_dataset.SetGeoTransform(input_dataset.GetGeoTransform())

# Reproject each raster band
band = input_dataset.GetRasterBand(1)
output_band = output_dataset.GetRasterBand(1)
output_band.SetNoDataValue(band.GetNoDataValue())

gdal.ReprojectImage(input_dataset, output_dataset, input_projection.ExportToWkt(), output_projection.ExportToWkt(), gdal.GRA_NearestNeighbour)

# Close datasets
input_dataset = None
output_dataset = None

print("Reprojection complete!")