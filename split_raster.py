import os
from osgeo import gdal
from osgeo import osr
from tqdm import tqdm
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory
from tkinter.simpledialog import askinteger

gdal.UseExceptions()

def split_raster(input_file, output_folder,dimension):
    
    # Open the input raster
    dataset = gdal.Open(input_file)
    if dataset is None:
        print("Failed to open the input raster.")
        return
    
    # Get the raster's geotransformation parameters
    geotransform = dataset.GetGeoTransform()
    origin_x = geotransform[0]
    origin_y = geotransform[3]
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]
    
    # Get the raster size (number of columns and rows)
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    
    # Calculate the number of output rasters in x and y directions
    num_x_rasters = cols // dimension
    num_y_rasters = rows // dimension
    
    # Create the output folder if it doesn't exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    
    # Get the input raster's spatial reference system (SRS)
    srs = osr.SpatialReference()
    srs.ImportFromWkt(dataset.GetProjection())
    
    # Create a geographic coordinate system (GCS) for Brazil (WGS 84)
    gcs = osr.SpatialReference()
    gcs.ImportFromEPSG(4326)  # WGS 84
    
    # Create a transformation from the input raster's SRS to the GCS
    transform_to_gcs = osr.CoordinateTransformation(srs, gcs)
    
    # Get the input raster's data type
    input_data_type = dataset.GetRasterBand(1).DataType
    
    # Loop through the output rasters and create each one
    for j in tqdm(range(num_y_rasters), desc="Processing"):
        for i in tqdm(range(num_x_rasters), desc="Processing", leave=False):
            # Calculate the extent of the output raster in the GCS
            x_min = origin_x + (i * dimension * pixel_width)
            y_min = origin_y + ((j + 1) * dimension * pixel_height)  # Adjust calculation for y_min
            x_max = x_min + (dimension * pixel_width)
            y_max = y_min - (dimension * pixel_height)  # Adjust calculation for y_max
            
            # Transform the coordinates of each corner of the output raster to the input raster's SRS
            x_min, y_min, _ = transform_to_gcs.TransformPoint(x_min, y_min)
            x_max, y_max, _ = transform_to_gcs.TransformPoint(x_max, y_max)
            
            # Create the output raster file path
            output_file = os.path.join(output_folder, f"raster_{i}_{j}.tif")
            
            # Create the output raster
            driver = gdal.GetDriverByName("GTiff")
            output_dataset = driver.Create(output_file, dimension, dimension, dataset.RasterCount, input_data_type)
            
            # Set the SRS and geotransformation of the output raster
            output_dataset.SetProjection(gcs.ExportToWkt())
            output_dataset.SetGeoTransform([x_min, pixel_width, 0, y_max, 0, pixel_height])
            
            # Loop through all bands in the input raster
            for band_num in range(1, dataset.RasterCount + 1):
                input_band = dataset.GetRasterBand(band_num)
                output_band = output_dataset.GetRasterBand(band_num)
                data = input_band.ReadAsArray(i * dimension, j * dimension, dimension, dimension)
                output_band.WriteArray(data)
            
            # Close the output raster
            output_dataset = None
    
    # Close the input raster
    dataset = None

# Example usage


if __name__ == "__main__":
    input_raster = askopenfilename(title='Selecione o raster de entrada')
    output_folder = askdirectory(title='Selecione a pasta de saída')
    dimension = askinteger(title=None,prompt='Selecione a dimensão em pixels')
    split_raster(input_raster, output_folder,dimension)