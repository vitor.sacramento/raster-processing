from cbers4asat import Cbers4aAPI
from datetime import date
from osgeo import ogr
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory

ogr.UseExceptions()

data_inicial = date(2022,6,12)
data_final = date(2023,6,12)
cloud_percent = 10
maximo_de_imagens = 1
collection_list = ['CBERS4A_WPM_L4_DN']
bands_list = ['red','green','blue']

api = Cbers4aAPI('sacramento.unb@gmail.com')

# load shapefile
shapefile_path = askopenfilename(
    title="Select your ROI",
    filetypes=[('Shapefile', '*.shp')]
)

# load shapefile driver and open the file
driver = ogr.GetDriverByName('ESRI Shapefile')
dataset = driver.Open(shapefile_path)
layer = dataset.GetLayer()

# select the output path
outdir = askdirectory(
    title="Select the output folder",
    initialdir="/home/vitor/Downloads"
)

# acquire the epsg from the shapefile
EPSG = int(layer.GetSpatialRef().GetAttrValue('AUTHORITY',1))

# get bounding box coordinates
if EPSG == 4326 or EPSG == 4674:
    extent = layer.GetExtent()
    minx, maxx, miny, maxy = extent
else:
    print('Shapefile not in WGS 84 Geographic(EPSG:4326)')
    exit()

# print bounding box coordinates
print(f"Bounding box coordinates: ({minx}, {miny}, {maxx}, {maxy})")

# close dataset
dataset = None
layer = None

# List containing the bounding box coordinates in the format [west_lon, south_lat, east_lon, north_lat]
bbox = [minx, miny, maxx, maxy]

produtos = api.query(
    location = bbox,
    initial_date = data_inicial,
    end_date = data_final,
    cloud = cloud_percent,
    limit = maximo_de_imagens,
    collections = collection_list
)

imagens = api.to_geodataframe(produtos, 'EPSG:4674')
print(imagens)

api.download(products=imagens, bands=bands_list,
            outdir=outdir, with_folder=False)