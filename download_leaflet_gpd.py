import os
from samgeo import tms_to_geotiff
from tkinter.filedialog import askopenfilenames
import geopandas as gpd 

shapefiles_path = askopenfilenames(
    title="Select your ROI",
    
)

index = 0

for shapefile_path in shapefiles_path:
    # Read the shapefile using GeoPandas
    gdf = gpd.read_file(shapefile_path)
    
    # acquire the epsg from the shapefile
    EPSG = int(gdf.crs.to_epsg())

    # get bounding box coordinates
    if EPSG == 4326 or EPSG == 4674:
        minx, miny, maxx, maxy = gdf.total_bounds
    else:
        print('Shapefile not in WGS 84 Geographic (EPSG:4326)')
        exit()

    # print bounding box coordinates
    print(f"Bounding box coordinates: ({minx}, {miny}, {maxx}, {maxy})")

    bbox = [minx, miny, maxx, maxy]

    image = os.path.join(os.path.dirname(shapefile_path),(str(index)+'_leaflet.tiff'))
    print(image)

    if not os.path.exists(os.path.dirname(image)):
        os.makedirs(os.path.dirname(image))
    
    try:    
        tms_to_geotiff(output=image, bbox=bbox, zoom=17, source='Satellite')
    except AttributeError as e:
        print(e)
    pass

    index += 1