import ee
from ee_plugin import Map

Estados = ee.FeatureCollection('users/sacramentounb/Brasil_estados')
Estado = Estados.filter(ee.Filter.eq('SIGLA_UF','MT'))

imagem = ee.ImageCollection('COPERNICUS/S2_HARMONIZED').filterDate('2021-05-01', '2021-07-15')
rgbVis = {'min': 200, 'max': 2000, 'bands': ['B4', 'B3', 'B2']}

Map.addLayer(imagem.median(), rgbVis, 'Brasil')
Map.addLayer(ee.Image().paint(Estado, 0, 2), {}, 'Estado')
Map.centerObject(Estado, 7)