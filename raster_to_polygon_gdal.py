import os
import osgeo.gdal as gdal
import osgeo.ogr as ogr
from tkinter.filedialog import askdirectory
from tqdm import tqdm

# set number of threads to use
os.environ['GDAL_NUM_THREADS'] = 'ALL_CPUS'

gdal.UseExceptions()

# input folder path
input_folder = askdirectory(title='Selecione a pasta de entrada')

# output folder path
output_folder = askdirectory(title='Selecione a pasta de saída')

# iterate over raster files in the input folder
for filename in tqdm(os.listdir(input_folder)):
    if filename.endswith('.tif') or filename.endswith('.tiff'):
        # input file path
        input_file = os.path.join(input_folder, filename)

        # output file path
        output_file = os.path.join(output_folder, os.path.splitext(filename)[0] + '.gpkg')

        # output layer name
        output_layer = os.path.splitext(os.path.basename(output_file))[0]

        # attribute field name
        attribute_field = 'DN'

        # open the input file
        input_ds = gdal.Open(input_file)

        # get the input band
        input_band = input_ds.GetRasterBand(1)

        # create a new vector file
        output_ds = ogr.GetDriverByName('GPKG').CreateDataSource(output_file)

        # create a new layer in the output file
        output_layer = output_ds.CreateLayer(output_layer, srs=None)

        # add a new attribute field to the layer
        attribute_defn = ogr.FieldDefn(attribute_field, ogr.OFTInteger)
        output_layer.CreateField(attribute_defn)

        # convert the raster to polygons
        gdal.Polygonize(input_band, None, output_layer, 0, [], callback=None)

        # clean up
        input_ds = None
        output_ds = None