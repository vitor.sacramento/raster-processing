from osgeo import gdal
from tkinter.filedialog import askopenfilename

gdal.SetConfigOption('NUM_THREADS', 'ALL_CPUS')

src_dataset = askopenfilename( 
    title="Select the TIFF file",
    filetypes=[('Geotiff', '*.tif')])

image = gdal.Open(src_dataset, 1)

image.BuildOverviews("AVERAGE")