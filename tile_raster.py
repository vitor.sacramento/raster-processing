from osgeo import gdal
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename

# Set the input raster file name
input_raster = askopenfilename(
    title="Select your input raster",
    filetypes=[('Geotiff', '*.tif')])

# Set the output tiled raster file name
output_tiled_raster = asksaveasfilename(
    title="Select your output raster",
    filetypes=[('Geotiff', '*.tif')]
)

# Set the tile size (in pixels)
tile_size_x = 256
tile_size_y = 256

# Open the input raster
in_ds = gdal.Open(input_raster)

# Get the size of the input raster
rows = in_ds.RasterYSize
cols = in_ds.RasterXSize

# Get the number of tiles needed in each dimension
num_tiles_x = int(cols / tile_size_x) + 1
num_tiles_y = int(rows / tile_size_y) + 1

# Create the output tiled raster
driver = gdal.GetDriverByName('GTiff')
out_ds = driver.Create(output_tiled_raster, cols, rows, 1, in_ds.GetRasterBand(1).DataType)

# Loop through each tile and copy it to the output tiled raster
for j in range(num_tiles_y):
    for i in range(num_tiles_x):
        # Compute the tile offset
        x_offset = i * tile_size_x
        y_offset = j * tile_size_y
        
        # Compute the tile size (use smaller size for last tile)
        x_tile_size = min(tile_size_x, cols - x_offset)
        y_tile_size = min(tile_size_y, rows - y_offset)
        
        # Read the tile from the input raster
        tile = in_ds.ReadAsArray(x_offset, y_offset, x_tile_size, y_tile_size)
        
        # Write the tile to the output tiled raster
        out_ds.GetRasterBand(1).WriteArray(tile, x_offset, y_offset)

# Set the geotransform and projection for the output tiled raster
out_ds.SetGeoTransform(in_ds.GetGeoTransform())
out_ds.SetProjection(in_ds.GetProjection())

# Close the input and output rasters
in_ds = None
out_ds = None
