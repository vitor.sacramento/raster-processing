from osgeo import gdal
from tkinter.filedialog import askopenfilename
import os

input_raster = askopenfilename( 
    title="Select the TIFF file",
    filetypes=[('Geotiff', '*.tif')])

def compress_raster(input_raster):

    output_raster = os.path.splitext(input_raster)[0]+'_LZW.tif'

    options = ['-co', 'COMPRESS=LZW', '-co', 'PREDICTOR=1', '-co', 'TILED=YES', '-co', 'BIGTIFF=YES',]

    try:
        gdal.Translate(output_raster, input_raster, options=options)
    except AttributeError as e:
        print(e)