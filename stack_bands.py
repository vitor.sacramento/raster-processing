from osgeo import gdal
from tkinter.filedialog import askopenfilename
import os

gdal.UseExceptions()

# Specify the input file paths
band1_file = askopenfilename(
    title="Select Band 1 - Blue Channel",
    filetypes=[('Geotiff', '*.tif')])

initial_dir = os.path.dirname(band1_file)

band2_file = askopenfilename(
    title="Select Band 2 - Green Channel",
    filetypes=[('Geotiff', '*.tif')],
    initialdir=initial_dir)

# Prompt the user to select the third file
band3_file = askopenfilename(
    title="Select Band 3 - Red Channel",
    filetypes=[('Geotiff', '*.tif')],
    initialdir=initial_dir)

# Open each band as a GDAL dataset
band1_ds = gdal.Open(band1_file)
band2_ds = gdal.Open(band2_file)
band3_ds = gdal.Open(band3_file)

# Get the dimensions and geotransform of one of the bands
width = band1_ds.RasterXSize
height = band1_ds.RasterYSize
geotransform = band1_ds.GetGeoTransform()

# Create a new GDAL dataset for the stacked output
driver = gdal.GetDriverByName('GTiff')
output_file = os.path.join(os.path.dirname(band1_file),os.path.splitext(os.path.basename(band1_file))[0]+'_RGB.tif')

output_ds = driver.Create(output_file, width, height, 3, gdal.GDT_Int16)

# Set the geotransform and projection of the output dataset
output_ds.SetGeoTransform(geotransform)
output_ds.SetProjection(band1_ds.GetProjection())

# Write each band into the output dataset
output_ds.GetRasterBand(1).WriteArray(band3_ds.ReadAsArray())
output_ds.GetRasterBand(2).WriteArray(band2_ds.ReadAsArray())
output_ds.GetRasterBand(3).WriteArray(band1_ds.ReadAsArray())

# Close the input and output datasets
band1_ds = None
band2_ds = None
band3_ds = None
output_ds = None