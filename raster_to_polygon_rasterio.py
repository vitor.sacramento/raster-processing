import rasterio
import geopandas as gpd
from concurrent.futures import ThreadPoolExecutor
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename

def raster_to_polygon(input_raster, output_gpkg, num_threads=None):
    with rasterio.open(input_raster) as src:
        shapes_gen = rasterio.features.shapes(
            src.read(1), 
            transform=src.transform, 
            connectivity=4, 
            mask=None, 
            all_touched=False
        )
        features = [
            {'geometry': shape, 'value': val} if len(shape) == 2 
            else {'geometry': shape[0], 'value': 0} 
            for shape, val in shapes_gen
        ]
    polygons = gpd.GeoDataFrame.from_features(features, crs=src.crs)
    polygons.to_file(output_gpkg, layer='polygons', driver='GPKG')

    return polygons

if __name__ == '__main__':
    input_raster = askopenfilename(title='Selecione o raster de entrada')
    output_gpkg = asksaveasfilename(title='selecione o arquivo de saída')
    num_threads = 'ALL_CPUS'
    
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        future = executor.submit(raster_to_polygon, input_raster, output_gpkg)
        future.result()