from osgeo import gdal
from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import time

gdal.UseExceptions()
gdal.SetConfigOption('NUM_THREADS', 'ALL_CPUS')

# Initialize Tkinter
root = Tk()
root.withdraw()

# Select input raster file
input_raster = askopenfilename(title='Selecione os rasters de entrada')

# Open the input raster dataset
src_dataset = gdal.Open(input_raster)

# Select output raster file
output_raster = asksaveasfilename(title='Selecione o raster de saída com extensão')

def to_UINT16(input_raster, output_raster):

    start_time = time.time()

    input_options = src_dataset.GetMetadata('IMAGE_STRUCTURE')
    output_options = ['-co', 'COMPRESS=' + input_options.get('COMPRESSION', 'LZW'),
                      '-ot', 'UInt16',
                      '-co', 'TILED=YES',
                      '-co', 'BIGTIFF=YES']

    # Translate the input raster to UINT16 format and save it to the output raster file
    g = gdal.Translate(output_raster, src_dataset, options=output_options)
    g = None

    print('Finished in ' + str(time.time() - start_time) + ' seconds')

to_UINT16(input_raster, output_raster)