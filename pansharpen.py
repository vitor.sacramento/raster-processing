import os
import time
from osgeo_utils.gdal_pansharpen import gdal_pansharpen
from tkinter import filedialog
from tkinter import simpledialog

# Set input and output file paths
pan_path = filedialog.askopenfilename(
    title="Select the PAN band",
    filetypes=[('Geotiff', '*.tif')]
)

if not os.path.isfile(pan_path):
    print(f"Error: Invalid PAN file {pan_path}")
    exit()

initial_dir = os.path.dirname(pan_path)

rgb_path = filedialog.askopenfilename(
    title="Select the RGB band",
    filetypes=[('Geotiff', '*.tif')],
    initialdir=initial_dir
)

if not os.path.isfile(rgb_path):
    print(f"Error: Invalid RGB file {rgb_path}")
    exit()

outdir = filedialog.askdirectory(
    title="Select the output directory",
    initialdir=initial_dir
)

if not os.path.isdir(outdir):
    print(f"Error: Invalid directory path {outdir}")
    exit()

output_file = (os.path.join(os.path.dirname(rgb_path),os.path.splitext(rgb_path)[0])+'_IHS.tif')

# Set RGB bands for output
rgb_bands = []
for i in range(3):
    band = simpledialog.askinteger(prompt = f'Enter the band number (1-3) for the RGB output in position {i+1}: ',title = None)
    rgb_bands.append(int(band))

# Execute pansharpening
start_time = time.time()

args = [
    '',
    '-b', str(rgb_bands[0]),
    '-b', str(rgb_bands[1]),
    '-b', str(rgb_bands[2]),
    '-threads', 'ALL_CPUS',
    pan_path,
    rgb_path,
    output_file
]

gdal_pansharpen(args)
print(f"Pansharpening complete. Output file saved to {output_file}")

print(f"Execution time: {time.time() - start_time:.4f} seconds")