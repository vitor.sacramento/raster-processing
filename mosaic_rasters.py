from osgeo import gdal
from tkinter.simpledialog import askinteger
from tkinter.filedialog import askopenfilename

def mosaic_images(num_images):
    # Create an empty list to store the file paths
    image_paths = []

    # Prompt the user for file paths
    for i in range(num_images):
        path = askopenfilename(title=f"Enter the file path for image {i+1}: ")
        image_paths.append(path)

    # Open the first image to get the properties
    first_image = gdal.Open(image_paths[0])
    driver = first_image.GetDriver()

    # Get the dimensions and projection of the first image
    cols = first_image.RasterXSize
    rows = first_image.RasterYSize
    projection = first_image.GetProjection()

    # Create the output mosaic
    output_mosaic = driver.Create("mosaic.tif", cols, rows, 3, gdal.GDT_Byte)
    output_mosaic.SetProjection(projection)

    # Loop through the images and copy their data into the mosaic
    for path in image_paths:
        image = gdal.Open(path)
        output_mosaic.WriteRaster(0, 0, cols, rows, image.ReadRaster(0, 0, cols, rows))

    # Clean up
    output_mosaic = None
    first_image = None

    print("Mosaic created successfully!")


# Prompt the user for the number of images to mosaic
num_images = askinteger(title=None,prompt='How many images do you want to mosaic?')

# Call the mosaic_images function
mosaic_images(num_images)