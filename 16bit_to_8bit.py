from osgeo import gdal
import numpy as np
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename


def convert_16bit_to_8bit(input_image, output_image):
    # Open the input image using GDAL
    dataset = gdal.Open(input_image, gdal.GA_ReadOnly)

    # Get the number of bands in the image
    num_bands = dataset.RasterCount

    # Read the input image data as a NumPy array
    image_data = dataset.ReadAsArray()

    # Check if the input image has 16 bits per pixel
    if dataset.GetRasterBand(1).DataType != gdal.GDT_UInt16:
        print("Input image does not have 16 bits per pixel.")
        return

    # Normalize the input image to the range of 0-255
    normalized_data = ((image_data / 65536) * 255)

    # Convert the normalized data to 8-bit integer values
    eight_bit_data = normalized_data.astype(np.uint8)

    # Create the output image using GDAL
    driver = gdal.GetDriverByName("GTiff")
    output_dataset = driver.Create(output_image, dataset.RasterXSize, dataset.RasterYSize, num_bands, gdal.GDT_Byte)

    # Write the 8-bit data to the output image bands
    for band_index in range(num_bands):
        output_band = output_dataset.GetRasterBand(band_index + 1)
        output_band.WriteArray(eight_bit_data[band_index, :, :])

    # Set the geotransform and projection from the input image
    output_dataset.SetGeoTransform(dataset.GetGeoTransform())
    output_dataset.SetProjection(dataset.GetProjection())

    # Close the datasets
    dataset = None
    output_dataset = None

    print("Conversion complete.")

# Example usage
input_image = askopenfilename(title='Selecione o raster 16bits de entrada')
output_image = asksaveasfilename(title='Selecione o raster 8bits de saída')

convert_16bit_to_8bit(input_image, output_image)