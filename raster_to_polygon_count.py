import os
import osgeo.gdal as gdal
import osgeo.ogr as ogr
from tkinter.filedialog import askdirectory
from tqdm import tqdm
from osgeo.ogr import OFTString, OFTInteger

gdal.UseExceptions()

# set number of threads to use
os.environ['GDAL_NUM_THREADS'] = 'ALL_CPUS'

# input folder path
input_folder = askdirectory(title='Selecione a pasta de entrada')

# output folder path
output_folder = input_folder

# initialize the index counter
index = 0

# iterate over raster files in the input folder
for filename in tqdm(os.listdir(input_folder), desc='Processing'):
    if filename.endswith('.tif') or filename.endswith('.tiff'):
        # input file path
        input_file = os.path.join(input_folder, filename)

        # output file path
        output_file = os.path.join(output_folder, os.path.splitext(filename)[0] + '.gpkg')

        # output layer name
        output_layer = os.path.splitext(os.path.basename(output_file))[0]

        # attribute field name
        attribute_field = 'DN'

        # open the input file
        input_ds = gdal.Open(input_file)

        # get the input band
        input_band = input_ds.GetRasterBand(1)

        # create a new vector file
        output_ds = ogr.GetDriverByName('GPKG').CreateDataSource(output_file)

        # create a new layer in the output file
        output_layer = output_ds.CreateLayer(output_layer)

        # add a new attribute field to the layer
        attribute_defn = ogr.FieldDefn(attribute_field, ogr.OFTInteger)
        output_layer.CreateField(attribute_defn)

        field_tipo = ogr.FieldDefn("idx", ogr.OFTInteger)
        try:
            output_layer.CreateField(field_tipo)
        except AttributeError as e:
            print(e)

        # convert the raster to polygons
        gdal.Polygonize(input_band, None, output_layer, 0, [], callback=None)

        # create a new geometry with the desired spatial reference
        output_srs = ogr.osr.SpatialReference()
        output_srs.ImportFromEPSG(4326)

        # iterate over features in the output layer
        for feature in output_layer:
            # copy the geometry to a new geometry object with the desired spatial reference
            geometry = feature.GetGeometryRef()
            new_geometry = geometry.Clone()
            new_geometry.AssignSpatialReference(output_srs)

            # update the feature with the new geometry
            feature.SetGeometry(new_geometry)

            # set the "idx" field with the unique index value
            feature.SetField("idx", index)

            output_layer.SetFeature(feature)

            # increment the index counter
            index += 1

        # clean up
        input_ds = None
        output_ds = None