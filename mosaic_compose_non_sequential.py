import cv2
import numpy as np
import os

def mosaic_images(image_folder, output_path):
    images = []
    for filename in sorted(os.listdir(image_folder)):
        if filename.endswith('.jpg') or filename.endswith('.png'):
            images.append(os.path.join(image_folder, filename))

    if len(images) == 0:
        print("No images found in the folder.")
        return

    # Load the first image to get dimensions
    first_image = cv2.imread(images[0])
    image_height, image_width = first_image.shape[:2]

    # Create a blank canvas to hold the mosaic
    mosaic = np.zeros((image_height, image_width), dtype=np.uint8)

    for i, image_path in enumerate(images):
        image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        result = cv2.matchTemplate(mosaic, image, cv2.TM_CCOEFF_NORMED)
        _, _, _, max_loc = cv2.minMaxLoc(result)
        mosaic[max_loc[1]:max_loc[1]+image_height, max_loc[0]:max_loc[0]+image_width] = image

    # Convert the mosaic back to color
    mosaic = cv2.cvtColor(mosaic, cv2.COLOR_GRAY2BGR)

    # Save the mosaic image
    cv2.imwrite(output_path, mosaic)

    print(f"Mosaic image saved to {output_path}.")

# Provide the path to the folder containing the images and the desired output path for the mosaic image
image_folder = 'path/to/images/folder'
output_path = 'path/to/output/mosaic.jpg'

mosaic_images(image_folder, output_path)