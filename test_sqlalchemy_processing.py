from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func


Base = declarative_base()

class AprtLimiteUtm(Base):
    __tablename__ = 'aprt_limite_utm'

    id = Column(Integer, primary_key=True)
    geom = Column(String)
    fazenda = Column(String)
    proprietar = Column(String)
    estado = Column(String)
    municipio = Column(String)

class ProdesCerradoAmz(Base):
    __tablename__ = 'prodes_cerrado_amz'

    id = Column(Integer, primary_key=True)
    geom = Column(String)
    image_date = Column(String)


class ProdesCerradoAmz(Base):
    __tablename__ = 'prodes_cerrado_amz'

    id = Column(Integer, primary_key=True)
    geom = Column(String)
    image_date = Column(String)

class DesmatArea(Base):
    __tablename__ = 'desmat_area'

    id = Column(Integer, primary_key=True)
    geom = Column(String)
    id_fazenda = Column(Integer)
    code = Column(String)
    image_date = Column(String)

class DesmatAreaUtm(Base):
    __tablename__ = 'desmat_area_utm'

    id = Column(Integer, primary_key=True)
    id_fazenda = Column(Integer)
    desmat_ha = Column(String)
    geom = Column(String)
    image_date = Column(String)


engine = create_engine('postgresql://user:password@host/dbname')
Session = sessionmaker(bind=engine)
session = Session()

desmat_area = (
    session.query(
        func.ST_Intersection(AprtLimiteUtm.geom, ProdesCerradoAmz.geom).label('geom'),
        AprtLimiteUtm.id_fazenda,
        AprtLimiteUtm.code,
        ProdesCerradoAmz.image_date
    )
    .join(ProdesCerradoAmz, func.ST_Intersects(AprtLimiteUtm.geom, ProdesCerradoAmz.geom))
    .group_by(AprtLimiteUtm.id_fazenda, AprtLimiteUtm.geom, ProdesCerradoAmz.geom, AprtLimiteUtm.code, ProdesCerradoAmz.image_date)
    .subquery('desmat_area')
)

session.execute(
    DesmatArea.__table__.insert().from_select(
        [
            DesmatArea.geom,
            DesmatArea.id_fazenda,
            DesmatArea.code,
            DesmatArea.image_date
        ],
        desmat_area
    )
)

session.commit()